print ("Hola mundo");


diccionario = {'nombre':'Fernando', 'edad':25, 'cursos':['Python','JS','C']};

print (diccionario['cursos']);
print (diccionario['nombre']);
print (diccionario['cursos'][1]);

for key in diccionario:
    print(key,":",diccionario[key]);

diccionario2 = diccionario.copy()
print(diccionario2)

valor = diccionario2.get('nombre')
print(valor)

valor2 = diccionario2.pop('cursos')
print(diccionario2)

#dict()
#zip()
#items()
#Keys()
#values()
#clear
#update()

items = diccionario.items()
print(items)